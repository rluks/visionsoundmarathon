#-------------------------------------------------
#
# Project created by QtCreator 2013-10-11T12:19:36
#
#-------------------------------------------------

QT       += core gui
QT += widgets

TARGET = Webka
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    escapi.cpp

HEADERS  += mainwindow.h \
    escapi.h

FORMS    += mainwindow.ui
