//#include <QtGui/QApplication> //qt 4
#include <QtWidgets/QApplication> //qt 5
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
