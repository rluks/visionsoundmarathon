#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->cam = new Camera(471,281);

    cam->initialize();


    cam->capture();
    QImage img(cam->getImage());

    ui->label->setPixmap(QPixmap::fromImage(img));
    cam->my_ui = ui;


}

MainWindow::~MainWindow()
{
    delete ui;
}

// ***********************


Camera::Camera(int width, int height, QObject *parent) :
    QObject(parent),
    width_(width),
    height_(height)
{
    capture_.mWidth = width;
    capture_.mHeight = height;
    capture_.mTargetBuf = new int[width * height];

    int devices = setupESCAPI();
    if (devices == 0)
    {
        qDebug() << "[Camera] ESCAPI initialization failure or no devices found";
    }

    timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(MySlot()));
    timer->start(50);
}

Camera::~Camera()
{
    deinitCapture(0);
}

int Camera::initialize()
{
    if (initCapture(0, &capture_) == 0)
    {
        qDebug() << "[Camera] Capture failed - device may already be in use";
        return -2;
    }



    return 0;
}

void Camera::deinitialize()
{
    deinitCapture(0);
}

int Camera::capture()
{
    doCapture(0);
    while(isCaptureDone(0) == 0);

    image_ = QImage(width_, height_, QImage::Format_ARGB32);
    for(int y(0); y < height_; ++y)
    {
        for(int x(0); x < width_; ++x)
        {
            int index(y * width_ + x);
            image_.setPixel(x, y, capture_.mTargetBuf[index]);
         }
    }
    return 1;
}

void Camera::MySlot()
{
    this->capture();
    QImage img(this->getImage());

    my_ui->label->setPixmap(QPixmap::fromImage(img));
}

// ***********************
/*
MyTimer::MyTimer()
{
    timer = new QTimer(this);

    Camera cam(640, 480);
    cam.initialize();
    cam.capture();
    QImage img(cam.getImage());

    //ui->label->setPixmap(QPixmap::fromImage(img));
    qDebug() << "ABC";

    connect(timer, SIGNAL(timeout()), this, SLOT(MySlot()));
    timer->start(1000);
}

void MyTimer::MySlot()
{
    qDebug() << "DEF";
}
*/

void MainWindow::on_pushButton_clicked()
{

    cam->capture();
    QImage img(cam->getImage());

    ui->label_2->setPixmap(QPixmap::fromImage(img));



}
