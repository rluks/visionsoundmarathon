#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QImage>
#include <QDebug>
#include <QTimer>
#include "escapi.h"


namespace Ui {
class MainWindow;
}

class Camera : public QObject
{
    Q_OBJECT

public:
    explicit Camera(int width, int height, QObject *parent = 0);
    ~Camera();
    int initialize();
    void deinitialize();
    int capture();
    const QImage& getImage() const { return image_; }
    const int* getImageRaw() const { return capture_.mTargetBuf; }

    QTimer * timer;
    Ui::MainWindow * my_ui;

private:
    int width_;
    int height_;
    struct SimpleCapParams capture_;
    QImage image_;

public slots:
    void MySlot();

};


class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Camera * cam;


private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};


/*class MyTimer : public QObject
{
    Q_OBJECT

public:
    MyTimer();
    QTimer *timer;
    Camera *cam;

public slots:
    void MySlot();

};*/

#endif // MAINWINDOW_H
